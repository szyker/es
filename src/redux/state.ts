import User from '../value-objects/User';

export const initialState = {
    user: {id: 123},
};

interface State {
    user: User;
}
export default State;
