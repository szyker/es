import * as React from 'react';
import {Text, View} from 'react-native';
import {NavigationStackScreenProps} from 'react-navigation-stack';

interface OwnProps {
    name: string;
}

type Props = OwnProps & NavigationStackScreenProps;

class HomeScreen extends React.PureComponent<Props> {
    static navigationOptions = {
        header: null,
    };

    render() {
        return (
            <View>
                <Text>Dashboard</Text>
            </View>
        );
    }
}

export default HomeScreen;
