export const primaryColour = '#75AFFF';
export const secondaryColour = '#5FBCE8';
export const tertiaryColour = '#5F74E8';
const violet = '#7A69FF';
const bright = '#69F6FF';

export const font = '#808E8D';
