export default interface Training {
    currentSkill: string;
    history: TrainingSession[];
}

interface TrainingSession {
    skill: string;
    change: number;
    date: string;
}
