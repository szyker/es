import * as React from 'react';
import {ScrollView, Text, View} from 'react-native';
import {NavigationStackScreenProps} from 'react-navigation-stack';
import {connect} from 'react-redux';
import Player from '../../value-objects/Player';
import PlayerListComponent from '../components/Team/PlayerListComponent';
import State from '../../redux/state';
import User from '../../value-objects/User';
import {getPlayersByCurrentUser} from '../../repositories/PlayerRepository';

interface OwnProps {
    user: User;
}

type Props = OwnProps & NavigationStackScreenProps;

interface OwnState {
    players: Player[];
    fetched: boolean;
}

class TeamScreen extends React.PureComponent<Props, OwnState> {
    static navigationOptions = () => {
        return {
            title: 'Lineup',
        };
    };

    constructor(props: Props) {
        super(props);
        this.state = {
            players: [],
            fetched: false,
        };
    }

    async componentDidMount(): Promise<Player[]> {
        const players = await getPlayersByCurrentUser();
        this.setState({players, fetched: true});
        return players;
    }

    render(): React.ReactNode {
        if (!this.state.fetched)
            return (
                <View>
                    <Text>Loading</Text>
                </View>
            );
        return (
            <ScrollView>
                <PlayerListComponent
                    players={this.state.players}
                    navigation={this.props.navigation as any}
                />
            </ScrollView>
        );
    }
}

const mapStateToProps = (state: State) => {
    return {
        user: state.user,
    };
};

const mapDispatchToProps = {};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(TeamScreen);
