import * as React from 'react';
import {View} from 'react-native';
import {NavigationStackScreenProps} from 'react-navigation-stack';
import Player from '../../value-objects/Player';
import PlayerProfileComponent from '../components/Player/PlayerProfileComponent';

interface OwnProps {
    player: Player;
}

type Props = OwnProps & NavigationStackScreenProps;

class PlayerScreen extends React.PureComponent<Props> {
    static navigationOptions = ({navigation}) => {
        return {
            title: navigation.getParam('playerName', 'Player details'),
        };
    };

    render() {
        return (
            <View style={{flex: 1}}>
                <PlayerProfileComponent
                    player={this.props.navigation.getParam('player')}
                />
            </View>
        );
    }
}

export default PlayerScreen;
