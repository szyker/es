import * as React from 'react';
import {StyleSheet, View} from 'react-native';
import Player from '../../../value-objects/Player';
import PlayerHeaderComponent from './PlayerHeaderComponent';
import PlayerSkillsComponent from './PlayerSkillsComponent';
import PlayerWeaponComponent from './PlayerWeaponComponent';
import PlayerTrainingComponent from './PlayerTrainingComponent';

interface Props {
    player: Player;
}

const styles = StyleSheet.create({
    view: {
        flex: 1,
        flexDirection: 'column',
    },
});

export default class PlayerProfileComponent extends React.PureComponent<Props> {
    render(): React.ReactNode {
        const {player} = this.props;
        return (
            <View style={styles.view}>
                <PlayerHeaderComponent player={player} />
                <PlayerSkillsComponent skills={player.skills} />
                <PlayerWeaponComponent weapons={player.weapons} />
                <PlayerTrainingComponent player={player} />
            </View>
        );
    }
}
