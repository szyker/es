import * as React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Weapons from '../../../value-objects/Weapons';

interface OwnProps {
    weapons: Weapons;
}

const styles = StyleSheet.create({
    weapons: {
        flex: 5,
        flexDirection: 'row',
    },
    row: {
        flex: 1,
        flexDirection: 'column',
    },
    weapon: {
        flex: 1,
    },
});

const PlayerWeaponComponent: React.FC<OwnProps> = ({weapons}: OwnProps) => {
    return (
        <View style={styles.weapons}>
            <View style={styles.row}>
                <View style={styles.weapon}>
                    <Text>Pistol: {weapons.pistol}</Text>
                </View>
                <View style={styles.weapon}>
                    <Text>Smg: {weapons.smg}</Text>
                </View>
                <View style={styles.weapon}>
                    <Text>Rifle: {weapons.rifle}</Text>
                </View>
            </View>
            <View style={styles.row}>
                <View style={styles.weapon}>
                    <Text>Sniper: {weapons.sniper}</Text>
                </View>
                <View style={styles.weapon}>
                    <Text>Utility: {weapons.utility}</Text>
                </View>
            </View>
        </View>
    );
};

export default PlayerWeaponComponent;
