import * as React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import PlayerSkills from '../../../value-objects/PlayerSkills';
import {SkillNames} from '../../../value-objects/Skills';

interface OwnProps {
    skills: PlayerSkills;
}

const styles = StyleSheet.create({
    skills: {
        flex: 5,
        flexDirection: 'row',
    },
    row: {
        flex: 1,
        flexDirection: 'column',
    },
    skill: {
        flex: 1,
    },
});

const PlayerSkillsComponent: React.FC<OwnProps> = ({skills}: OwnProps) => {
    return (
        <View style={styles.skills}>
            <View style={styles.row}>
                <View style={styles.skill}>
                    <Text>
                        {SkillNames.averageSkill}: {skills.averageSkill}
                    </Text>
                </View>
                <View style={styles.skill}>
                    <Text>
                        {SkillNames.intelligence}: {skills.intelligence}
                    </Text>
                </View>
                <View style={styles.skill}>
                    <Text>
                        {SkillNames.tactic}: {skills.tactic}
                    </Text>
                </View>
                <View style={styles.skill}>
                    <Text>
                        {SkillNames.calmness}: {skills.calmness}
                    </Text>
                </View>
            </View>
            <View style={styles.row}>
                <View style={styles.skill}>
                    <Text>
                        {SkillNames.recoilControl}: {skills.recoilControl}
                    </Text>
                </View>
                <View style={styles.skill}>
                    <Text>
                        {SkillNames.reflex}: {skills.reflex}
                    </Text>
                </View>
                <View style={styles.skill}>
                    <Text>
                        {SkillNames.aim}: {skills.aim}
                    </Text>
                </View>
                <View style={styles.skill}>
                    <Text>
                        {SkillNames.gameSens}: {skills.gameSens}
                    </Text>
                </View>
            </View>
        </View>
    );
};

export default PlayerSkillsComponent;
