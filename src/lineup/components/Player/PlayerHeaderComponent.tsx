import {StyleSheet, Text, View} from 'react-native';
import * as React from 'react';
import Player from '../../../value-objects/Player';

interface OwnProps {
    player: Player;
}

const styles = StyleSheet.create({
    header: {
        flex: 2,
        flexDirection: 'row',
    },
    avatar: {
        flex: 2,
    },
    basicInfo: {
        flex: 3,
        flexDirection: 'column',
    },
    nameInfoRow: {
        flex: 1,
    },
});

const PlayerHeaderComponent: React.FC<OwnProps> = ({player}: OwnProps) => {
    return (
        <View style={styles.header}>
            <View style={styles.avatar}>
                <Text>{player.avatar}</Text>
            </View>
            <View style={styles.basicInfo}>
                <View style={styles.nameInfoRow}>
                    <Text>
                        Name: {player.firstName} {player.lastName}
                    </Text>
                </View>
                <View style={styles.nameInfoRow}>
                    <Text>Nickname: {player.nickName}</Text>
                </View>
                <View style={styles.nameInfoRow}>
                    <Text>Age {player.age}</Text>
                </View>
            </View>
        </View>
    );
};

export default PlayerHeaderComponent;
