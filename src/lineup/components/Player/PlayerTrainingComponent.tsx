import * as React from 'react';
import {Picker, StyleSheet, Text, View} from 'react-native';
import Player from '../../../value-objects/Player';
import Training from '../../value-objects/Training';
import {
    getTraining,
    updateTraining,
} from '../../../repositories/TrainingRepository';
import Skills from '../../../value-objects/Skills';

interface OwnProps {
    player: Player;
}

interface State {
    training?: Training;
}

const styles = StyleSheet.create({
    component: {
        flex: 1,
        flexDirection: 'row',
    },
});

class PlayerTrainingComponent extends React.PureComponent<OwnProps, State> {
    constructor(props: OwnProps) {
        super(props);
        this.state = {
            training: null,
        };
    }

    async componentDidMount(): Promise<Training> {
        const training = await getTraining(this.props.player);
        this.setState({training});
        return training;
    }

    private updateTraining = (itemValue: string): void => {
        updateTraining(this.props.player, itemValue);
        this.setState(prevState => {
            prevState.training.currentSkill = itemValue;
            return {
                training: {...prevState.training, currentSkill: itemValue},
            };
        });
    };

    render(): React.ReactNode {
        if (this.state.training === null)
            return <View style={styles.component} />;
        const skills = new Skills();
        const pickerItems = [];
        for (const skill of skills) {
            pickerItems.push(
                <Picker.Item label={skill.name} value={skill.value} />,
            );
        }
        return (
            <View style={styles.component}>
                <Text style={{flex: 1}}>Currently training: </Text>
                <Picker
                    selectedValue={this.state.training.currentSkill}
                    style={{flex: 2}}
                    onValueChange={this.updateTraining}>
                    {pickerItems}
                </Picker>
            </View>
        );
    }
}

export default PlayerTrainingComponent;
