import * as React from 'react';
import {NavigationStackProp} from 'react-navigation-stack';
import {StyleSheet, Text, View} from 'react-native';
import Player from '../../../value-objects/Player';
import PlayerRowComponent from './PlayerRowComponent';

interface OwnProps {
    players: Player[];
    navigation: NavigationStackProp;
}

const styles = StyleSheet.create({
    list: {flex: 1, backgroundColor: '#fff'},

    head: {height: 40, backgroundColor: '#f1f8ff', flexDirection: 'row'},
    headerText: {fontWeight: 'bold', margin: 6},

    row: {flex: 1, flexDirection: 'row', backgroundColor: '#ededed'},
    textCell: {flex: 2},
    defaultCell: {flex: 1},
    text: {margin: 6},
});

const PlayerListComponent: React.FC<OwnProps> = ({players, navigation}: OwnProps) => {
    const playerRows = [];
    for (const player of players) {
        playerRows.push(
            <PlayerRowComponent
                key={player.id}
                player={player}
                navigation={navigation as any}
                styles={styles}
            />,
        );
    }

    return (
        <View style={styles.list}>
            <View style={styles.head}>
                <View style={styles.textCell}>
                    <Text style={styles.headerText}>Nickname</Text>
                </View>
                <View style={styles.defaultCell}>
                    <Text style={styles.headerText}>Age</Text>
                </View>
                <View style={styles.defaultCell}>
                    <Text style={styles.headerText}>Av. skill</Text>
                </View>
                <View style={styles.defaultCell}>
                    <Text style={styles.headerText}>Form</Text>
                </View>
                <View style={styles.textCell}>
                    <Text style={styles.headerText}>Best position</Text>
                </View>
            </View>
            {playerRows}
        </View>
    );
};

export default PlayerListComponent;
