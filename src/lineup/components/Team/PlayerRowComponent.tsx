import * as React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import {NavigationStackProp} from 'react-navigation-stack/src/types';
import Player from '../../../value-objects/Player';

interface OwnProps {
    player: Player;
    navigation: NavigationStackProp;
    styles: Styles;
}

interface Styles {
    row: any;
    textCell: any;
    defaultCell: any;
    text: any;
}

const PlayerRowComponent: React.FC<OwnProps> = ({
    player,
    navigation,
    styles,
}: OwnProps) => {
    return (
        <TouchableOpacity
            onPress={() =>
                navigation.push('Player', {
                    player,
                })
            }>
            <View style={styles.row}>
                <View style={styles.textCell}>
                    <Text style={styles.text}>{player.nickName}</Text>
                </View>
                <View style={styles.defaultCell}>
                    <Text style={styles.text}>{player.age}</Text>
                </View>
                <View style={styles.defaultCell}>
                    <Text style={styles.text}>
                        {player.skills.averageSkill}
                    </Text>
                </View>
                <View style={styles.defaultCell}>
                    <Text style={styles.text}>{player.form}</Text>
                </View>
                <View style={styles.textCell}>
                    <Text style={styles.text}>{player.bestPosition}</Text>
                </View>
            </View>
        </TouchableOpacity>
    );
};

export default PlayerRowComponent;
