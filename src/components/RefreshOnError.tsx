import * as React from 'react';
import {Button, Text, View} from 'react-native';

interface Props {
    refresh: () => {};
}

class RefreshOnError extends React.PureComponent<Props> {
    render(): React.ReactElement {
        return (
            <View>
                <Text>Error Happened (dev is already informed)</Text>
                <Button title="Try again" onPress={this.props.refresh} />
            </View>
        );
    }
}
