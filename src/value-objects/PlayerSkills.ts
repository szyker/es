interface PlayerSkills {
    averageSkill: number;
    aim: number;
    reflex: number;
    recoilControl: number;
    calmness: number;
    gameSens: number;
    tactic: number;
    intelligence: number;
}

export default PlayerSkills;
