export default interface Weapons {
    sniper: number;
    rifle: number;
    smg: number;
    pistol: number;
    utility: number;
}
