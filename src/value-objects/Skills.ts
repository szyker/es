export enum SkillNames {
    aim = 'Aim',
    reflex = 'Reflex',
    recoilControl = 'Recoil Control',
    calmness = 'Calmness',
    gameSens = 'GameSens',
    tactic = 'Tactic',
    intelligence = 'Intelligence',
    averageSkill = 'Average Skill',
}

enum AvailableSkills {
    aim = 'aim',
    reflex = 'reflex',
    recoilControl = 'recoilControl',
    calmness = 'calmness',
    gameSens = 'gameSens',
    tactic = 'tactic',
    intelligence = 'intelligence',
}

interface Skill {
    value: string;
    name: string;
}

class Skills implements IterableIterator<Skill> {
    private skills: Skill[] = [];
    private pointer: number = 0;

    constructor() {
        for (const item in AvailableSkills) {
            this.skills.push({
                value: AvailableSkills[item],
                name: SkillNames[item],
            });
        }
    }

    public next(): IteratorResult<Skill> {
        if (this.pointer < this.skills.length) {
            return {
                done: false,
                value: this.skills[this.pointer++],
            };
        }
        return {
            done: true,
            value: null,
        };
    }

    [Symbol.iterator](): IterableIterator<Skill> {
        return this;
    }
}

export default Skills;
