import PlayerSkills from './PlayerSkills';
import Weapons from './Weapons';

interface Player {
    id: string;
    firstName: string;
    lastName: string;
    nickName: string;
    age: number;
    country: string;
    leadership: number;
    form: number;
    bestPosition: string;
    avatar: string;
    skills: PlayerSkills;
    weapons: Weapons;
}

export default Player;
