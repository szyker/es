import {createStackNavigator} from 'react-navigation-stack';
import PlayerScreen from '../lineup/screens/PlayerScreen';
import TeamScreen from '../lineup/screens/TeamScreen';

export default createStackNavigator(
    {
        Player: {
            screen: PlayerScreen,
        },
        Team: {
            screen: TeamScreen,
        },
    },
    {
        initialRouteName: 'Team',
    },
);
