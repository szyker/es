import {createBottomTabNavigator} from 'react-navigation-tabs';
import {View} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {createAppContainer} from 'react-navigation';
import * as React from 'react';
import HomeStackNavigator from './HomeStackNavigator';
import TeamStackNavigator from './TeamStackNavigator';

const AppNavigator = createBottomTabNavigator(
    {
        Home: {
            screen: HomeStackNavigator,
            navigationOptions: {
                tabBarLabel: <View />,
                tabBarIcon: ({tintColor}) => (
                    <Icon name="home" size={30} color={tintColor} />
                ),
            },
        },
        Lineup: {
            screen: TeamStackNavigator,
            navigationOptions: {
                tabBarLabel: <View />,
                tabBarIcon: ({tintColor}) => (
                    <Icon name="users" size={30} color={tintColor} />
                ),
            },
        },
    },
    {
        initialRouteName: 'Home',
        tabBarOptions: {
            activeTintColor: '#D4AF37',
            inactiveTintColor: 'gray',
            style: {
                backgroundColor: 'white',
            },
            showIcon: true,
        },
    },
);

const Navigation = createAppContainer(AppNavigator);

export default Navigation;
