import Player from '../value-objects/Player';
import User from '../value-objects/User';

export async function getPlayersByUser(user: User): Promise<Player[]> {
    const response = await fetch('http://10.0.2.2:3000/players');
    return response.json();
}

export async function getPlayersByCurrentUser(): Promise<Player[]> {
    return getPlayersByUser({} as any);
}
