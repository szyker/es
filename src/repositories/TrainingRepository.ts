import Player from '../value-objects/Player';
import Training from '../lineup/value-objects/Training';

export async function getTraining(player: Player): Promise<Training> {
    const response = await fetch(`http://10.0.2.2:3000/training/${player.id}`);
    return response.json();
}

export function updateTraining(player: Player, skill: string): void {
    fetch(`http://10.0.2.2:3000/training/${player.id}`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            currentSkill: skill,
        }),
    });
}
