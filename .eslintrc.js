module.exports = {
    extends: [
        '@react-native-community',
        'airbnb-typescript',
        'prettier',
        'prettier/@typescript-eslint',
        'prettier/react',
    ],
    parser: '@typescript-eslint/parser',
    plugins: ["@typescript-eslint"],
    rules: {
        "no-undef": "off",
        "react/destructuring-assignment": "off",
        "no-restricted-syntax": "off",
        "guard-for-in": "off",
        "lines-between-class-members": ["error", "always", { exceptAfterSingleLine: true }],
        "no-plusplus": ["off"],
        "no-param-reassign": ["off"],
    }
};
