import * as express from 'express';
import * as faker from 'faker';

const app = express();
const port = 3000;

function createPlayers() {
    const players = [];
    for (let i = 1; i < 9; i++) {
        players.push({
            id: i,
            firstName: faker.name.findName(),
            lastName: faker.name.lastName(),
            nickName: faker.internet.userName(),
            age: faker.random.number({min: 16, max: 39}),
            country: faker.address.country(),
            leadership: faker.random.number({min: 1, max: 99}),
            form: faker.random.number({min: 1, max: 99}),
            bestPosition: faker.random.arrayElement(['Entry fragger', 'Rifler', 'Sniper', 'Support', 'Lurker']),
            avatar: 'jpg.png',
            skills: {
                averageSkill: faker.random.number({min: 1, max: 99}),
                aim: faker.random.number({min: 1, max: 99}),
                reflex: faker.random.number({min: 1, max: 99}),
                recoilControl: faker.random.number({min: 1, max: 99}),
                calmness: faker.random.number({min: 1, max: 99}),
                gameSens: faker.random.number({min: 1, max: 99}),
                tactic: faker.random.number({min: 1, max: 99}),
                intelligence: faker.random.number({min: 1, max: 99}),
            },
            weapons: {
                sniper: faker.random.number({min: 1, max: 99}),
                rifle: faker.random.number({min: 1, max: 99}),
                smg: faker.random.number({min: 1, max: 99}),
                pistol: faker.random.number({min: 1, max: 99}),
                utility: faker.random.number({min: 1, max: 99}),
            },
        });
    }

    return players;
}

function createTraining() {
    const history = [];
    const date = new Date();
    for (let i = 1; i < 99; i++) {
        date.setDate(date.getDate() - 1);
        history.push({
            skill: faker.random.arrayElement(['aim', 'reflex', 'recoilControl', 'calmness', 'gameSens', 'tactic', 'intelligence']),
            change: faker.random.number({min: -2, max: 5}),
            date: date.toISOString(),
        });
    }
    return {
        currentSkill: faker.random.arrayElement(['aim', 'reflex', 'recoilControl', 'calmness', 'gameSens', 'tactic', 'intelligence']),
        history,
    };
}

const players = createPlayers();

app.get('/players', (req: express.Request, res: express.Response) => {
    res.send(players);
});

app.get('/players/:id', (req: express.Request, res: express.Response) => {
    res.send(
        players.find(player => (Number(req.params.id) === player.id ? player : null)),
    );
});

app.get('/training/:id', (req: express.Request, res: express.Response) => {
    res.send(createTraining());
});

app.listen(port, () => {
    console.log(`server started at http://localhost:${port}`);
});
