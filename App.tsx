import * as React from 'react'
import { Provider, connect } from 'react-redux';
import { createStore } from 'redux';
import { initialState } from './src/redux/state';
import Navigation from './src/navigation/AppNavigator';

function reducer(state = initialState, action) {
    switch (action.type) {
        default:
            return state;
    }
}
const store = createStore(reducer, initialState);

class App extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <Navigation />
            </Provider>
        );
    }
}

export default App;
